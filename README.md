## Pong - Linux

#### Setup

Crear el direcotorio llamado `build`. Entrar y ejecutar en el terminal los sigueintes comandos:
```
`cmake ..`
`make`
```

Se creará el directorio llamado `src`. Entrar y ejecutar en el terminal los sigueintes comandos:
```
./logger
./loggerFIFO
./servidor
./bot
./cliente

```

Escribir un nombre y listo para jugar.

#### Rules
```
1- Intentar hacer rebotar la esfera con la raqueta.
2- Movimiento raqueta jugador1: Arriba letra 'w', Abajo letra 's'.
   Movimiento raqueta jugador2: Arriba letra 'o', Abajo letra 'l'.
3- Si la esfera toca la pared inmediatamente detrás de la raqueta, recibirá un punto el jugador contrario.
4- Cuando cualquiera de los dos jugadores alcanza 3 punto, aparece una segunda esfera.
5- Cuando cualquiera de los dos jugadores alcanza los 5 puntos, las esferas se convierten en esferas pulsantes (varían su radio constantemente).
6- Las raquetas pueden disparar (jugador1 letra 'd', jugador2 letra 'k').
7- Si un jugador alcanza al contrario con un disparo, el jugador que reciba el disparo reducirá la longitud de su raqueta un 40% del tamaño original.
8- El juego termina cuando uno de los dos jugadores llegue a 10 puntos.
9- Existe un modo Bot (jugador artificial), la raqueta 1 jugará automaticamente.
10- El Bot tomará el control en caso de que el jugador 2 no presione ninguna tecla durante 10 segundos, para retomar el control solo hay que presionar cualquier tecla de acción de la raqueta 2.
```

